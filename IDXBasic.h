/**
* @file IDXBasic.h
* @author Boyan M. Atanasov
* @date 26/01/2018
* @brief Basic structures and functionality, needed to process MNIST DB IDX3/1 files.
* This header file defines intrinsics, like the MNIST DB IDX3/1 file magic numbers,
* descriptor sizes and structure, and declares functionality which can be used to
* process and separate information from the files.
* @see http://yann.lecun.com/exdb/mnist/
*/

#ifndef MNIST_IDX_INTERPRETER_H
#define MNIST_IDX_INTERPRETER_H

#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <fstream>
#include <iostream>

/** @brief Defines the magic number identifier of an IDX3 file. The 32-bit number identifier is in high endian format. */
#define IDX3_MAGIC (0x00000803) 

/** @brief Defines the magic number identifier of an IDX1 file. The 32-bit number identifier is in high endian format. */
#define IDX1_MAGIC (0x00000801)

/** @brief Defines the size of the IDX3 descriptor header in <b>bytes</b>. */
#define IDX3_HEADER_WIDTH 16

/** @brief Defines the size of the IDX1 descriptor header in <b>bytes</b>. */
#define IDX1_HEADER_WIDTH 8

/**
 * @brief The IDXRead namespace contains all structures
 * and functionality needed to read the raw data in
 * an MNIST IDX file.
 * @see http://yann.lecun.com/exdb/mnist/
 */
namespace IDXRead {
	
	/**
	 * @brief IDXRESULT is returned by all functions in namespace IDXRead to indicate function outcome.
	 * 
	 */
#ifdef _DLL_BUILD
	typedef enum __declspec(dllexport) IDXRESULT {
#else
	typedef enum IDXRESULT {
#endif
		/// Normal return after correctly performed operation
		IDX_OK = 0,
		/// Generic error
		IDX_ERROR,
		/// File structure does not match the expected IDX structure
		IDX_BAD_FILE,
		/// File headers suggest file size different than the real one
		IDX_BAD_FILESIZE,
		/// File could not be found
		IDX_MISSING_FILE,
		/// IDX Descriptor is in disagreement with extracted data
		IDX_DESCRIPTOR_MISMATCH,
	} IDXRESULT;

	/**
	 * @brief (Image carrier) descriptor header structure, with extra fields.
	 * The real IDX3 descriptor header contains only the magic number,
	 * number of items the data must be split into, and the dimensions (rows and columns)
	 * of the data. The descriptor structure contains an std\::string to be
	 * populated with the path to the file, and a void pointer to the
	 * extractable bulk data in the IDX3 file.
	 * 
	 */
#ifdef _DLL_BUILD
	typedef struct __declspec(dllexport) {
#else
	typedef struct {
#endif
		std::string filepath;
		uint32_t magic_number;
		uint32_t num_items;
		uint32_t num_rows;
		uint32_t num_cols;
	} IDX3_Descriptor_t;
	
	/**
	 * @brief (Label carrier) descriptor header structure, with extra fields.
	 * The real IDX1 descriptor header contains only the magic number and the
	 * number of items the data must be split into. The descriptor structure contains an std\::string to be
	 * populated with the path to the file, and a void pointer to the
	 * extractable bulk data in the IDX1 file.
	 * 
	 */
#ifdef _DLL_BUILD
	typedef struct __declspec(dllexport) {
#else
	typedef struct {
#endif
		std::string filepath;
		uint32_t magic_number;
		uint32_t num_items;
	} IDX1_Descriptor_t;
	
	/**
	 * @brief MNIST Image data structure, with extra fields.
	 * The structure contains two descriptors for the image dimensions,
	 * with row_dx coressponding to rows/x-axis, and col_dy coressponding to
	 * columns/y-axis, and a pointer to the raw data.
	 */
#ifdef _DLL_BUILD
	typedef struct __declspec(dllexport) {
#else
	typedef struct {
#endif
		uint32_t row_dx;
		uint32_t col_dy;
		uint8_t * bytes;
	} MNIST_Image_t;
	
	/**
	 * @brief MNIST Label type definition.
	 * The type definition is purely introduced for consistency. The MNIST Label
	 * data is exactly one byte wide, thus this type is interchangeable with all other
	 * unsigned byte types (e.g. <b>unsigned char</b>, <b>uint8_t</b>).
	 */
#ifdef _DLL_BUILD
	typedef __declspec(dllexport) {
#else
	typedef
#endif
		uint8_t MNIST_Label_t;
	

	/**
	 * @brief Opens an IDX3 binary and populates an IDX3_Descriptor_t entity.
	 * 
	 * @param fpath (std\::string) The filepath to an IDX3 binary
	 * @param idx3_d A pointer to an IDX3_Descriptor_t entity or NULL upon error.
	 * @return #IDXRESULT
	 */
#ifdef _DLL_BUILD
	IDXRESULT __declspec(dllexport)
#else
	IDXRESULT
#endif
		idx3_fopen(std::string fpath, IDX3_Descriptor_t* idx3_d);

	/**
	 * @brief Opens an IDX1 binary and populates an IDX1_Descriptor_t entity.
	 * 
	 * @param fpath (std\::string) The filepath to an IDX1 binary
	 * @param idx1_d A pointer to an IDX1_Descriptor_t entity, or NULL upon error.
	 * @return #IDXRESULT
	 */
#ifdef _DLL_BUILD
	IDXRESULT __declspec(dllexport)
#else
	IDXRESULT
#endif
		idx1_fopen(std::string fpath, IDX1_Descriptor_t* idx1_d);

	/**
	* @brief Populates a necessary length of MNIST_Image_t entities.
	* The function creates, and allocates (via calloc()) memory for the necessary
	* number of MNIST_Image_t entities, and populates them with the image dimensions
	* and data.
	* @param idx3_d A populated IDX3_Descriptor_t entity.
	* @param images An empty reference to be pointed to the MNIST_Image_t entities
	* @return #IDXRESULT
	*/
#ifdef _DLL_BUILD
	IDXRESULT __declspec(dllexport)
#else
	IDXRESULT
#endif
		idx3_iexport(MNIST_Image_t* images, IDX3_Descriptor_t* idx3_d);

	/**
	* @brief Populates a necessary length of MNIST_Label_t entities.
	* The function creates, and allocates (via calloc()) memory for the necessary
	* number of MNIST_Label_t entities, and populates them with data.
	* @param idx1_d A populated IDX1_Descriptor_t entity.
	* @param labels An empty reference to be pointed to the MNIST_Label_t entities
	* @return #IDXRESULT
	*/
#ifdef _DLL_BUILD
	IDXRESULT __declspec(dllexport)
#else
	IDXRESULT
#endif		
		idx1_iexport(MNIST_Label_t* labels, IDX1_Descriptor_t* idx1_d);

}



#endif //MNIST_IDX_INTERPRETER_H
