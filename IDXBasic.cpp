/**
* @file IDXBasic.cpp
* @author Boyan M. Atanasov
* @date 27/01/2018
* @brief Basic structures and functionality, needed to process MNIST DB IDX3/1 files.
* This source file defines functionality which can be used to 
* process and separate information from the files.
* @see http://yann.lecun.com/exdb/mnist/
*/

#include "IDXBasic.h"

IDXRead::IDXRESULT IDXRead::idx3_fopen(std::string fpath, IDX3_Descriptor_t* idx3_d) {
	IDXRESULT IDXres;
	std::ifstream IDXfile;

	IDXfile.open(fpath, std::ifstream::binary);

	if (!IDXfile) {
		IDXres = IDXRESULT::IDX_MISSING_FILE;
		idx3_d = NULL;
		return IDXres;
	}
	else IDXres = IDXRESULT::IDX_OK;

	idx3_d = (IDX3_Descriptor_t*)malloc(sizeof(IDX3_Descriptor_t*));
	idx3_d->filepath = fpath;

	IDXfile.seekg(0, std::ios_base::beg);

	IDXfile.read(reinterpret_cast<char*>(&(idx3_d->magic_number)), sizeof(idx3_d->magic_number));
	IDXfile.read(reinterpret_cast<char*>(&(idx3_d->num_items)), sizeof(idx3_d->num_items));
	IDXfile.read(reinterpret_cast<char*>(&(idx3_d->num_rows)), sizeof(idx3_d->num_rows));
	IDXfile.read(reinterpret_cast<char*>(&(idx3_d->num_cols)), sizeof(idx3_d->num_cols));

	if (idx3_d->magic_number != (uint32_t)IDX3_MAGIC 
		|| idx3_d->num_items <= 0
		|| idx3_d->num_rows <= 0
		|| idx3_d->num_cols <= 0 ) {

		IDXres = IDXRESULT::IDX_BAD_FILE;
	}
	else IDXres = IDXRESULT::IDX_OK;

	IDXfile.close();

	return IDXres;
}